package xyz.zinglizingli.books.mapper;

import org.apache.ibatis.annotations.Param;
import xyz.zinglizingli.books.po.Book;
import xyz.zinglizingli.books.po.BookExample;

import java.util.List;

public interface BookMapper {
    int countByExample(BookExample example);

    int deleteByExample(BookExample example);

    int deleteByPrimaryKey(Long id);

    int insert(Book record);

    int insertSelective(Book record);

    List<Book> selectByExample(BookExample example);

    Book selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") Book record, @Param("example") BookExample example);

    int updateByExample(@Param("record") Book record, @Param("example") BookExample example);

    int updateByPrimaryKeySelective(Book record);

    int updateByPrimaryKey(Book record);

    List<Book> search(@Param("keyword") String keyword, @Param("catId") Integer catId);

    void addVisitCount(@Param("bookId") Long bookId);

    List<String> queryNewstBookIdList();

    List<String> queryEndBookIdList();

    List<String> queryNoEndBookIdList();
}